set nocompatible 

" Disable stupid shit
" Half of the useful stuff in this file is trying to prevent the editor from trying to be too clever
:autocmd BufReadPre,BufNewFile * let b:did_ftplugin = 1
set nohlsearch
set nosmartindent
set nocindent
filetype indent off
:set formatoptions-=cro
syntax off

" settings
set encoding=utf-8
set ttyfast
set showcmd
set ruler
set path+=**
set backupdir=~/.saves/backup//,.
set directory=~/.saves/swap//,.
set undodir=~/.saves/undo//,.
set undofile
"set wrap
set ignorecase
set smartcase
set incsearch
set autoindent
set hidden
set wildmenu
set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab 

" mapings
nmap <leader>\ :!clear ; make debug ; ./build.sh<CR>
nmap <leader>c :!wc2 '%'<CR>
nmap <leader>s :set spell<CR>
nmap Y y$
set backspace=indent,eol,start
vmap <BS> <gv
vmap <TAB> >gv

" cosmetic
set guicursor=
set laststatus=0
let g:netrw_banner=0
hi clear SpellBad
hi SpellBad cterm=underline
highlight Visual cterm=NONE ctermbg=3 ctermfg=0 gui=NONE
"set t_Co=16

" Set spelling automatically for markdown files
augroup markdownSpell
    autocmd!
    autocmd FileType markdown setlocal spell
augroup END
