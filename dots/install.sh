#!/bin/sh -e
# WARNING!!! THIS WILL WRITE OVER YOUR FILES!!! AAAAAA BE CAREFUL

if [ "$1" != "really" ] ; then 
	echo "WARNING: this program will overwrite your config files to make symlinks to the files in this directory. Make sure you know EXACTLY what it's going to do before running it with './install.sh really'"
	exit 0
fi

mkdir -p "$HOME/.local"
mkdir -p "$HOME/.config/nvim"
mkdir -p "$HOME/.config/i3status"
mkdir -p "$HOME/.config/i3"
mkdir -p "$HOME/.newsboat"

cp -f fzf-bash-keys.bash "$HOME/.local"
cp -f my_i3status.sh "$HOME/.local"
cp -f wallpaper.jpg "$HOME/.local"

ln -sf "$PWD/bashrc" "$HOME/.bashrc"
ln -sf "$PWD/tmux.conf" "$HOME/.tmux.conf"
ln -sf "$PWD/init.vim" "$HOME/.config/nvim/init.vim"
ln -sf "$PWD/init.vim" "$HOME/.config/nvim/init.vim"
ln -sf "$PWD/i3status-config" "$HOME/.config/i3status/config"
ln -sf "$PWD/i3-config" "$HOME/.config/i3/config"
ln -sf "$PWD/newsboat" "$HOME/.newsboat/config"
