/* Better wc(1) that gives you more information in a (visually) nicer format */

/* Usage: wc2 <filenames> */
/* Can also take stdin if there's no arguments */

#define FJ_IMPL
#include "fj.h"

enum {
	WITH_FILE,
	WITH_STDIN,
	WITH_TOTAL
};

struct counts {
	ulong bytes;
	ulong words;
	ulong lines;
	ulong non_blank_lines;
	ulong longest_line_byte;
	ulong longest_line_word;
};

struct counts total = {0};

static void report(char *filename, bool print_with_filename, struct counts *counts);
static void count(char *filename, int with, bool print_with_filename);

static void report(char *filename, bool print_with_filename, struct counts *counts) {
	if (print_with_filename) printf("%s:\n", filename);

	if (print_with_filename) printf(" ");
	printf("lines:%lu non-blank:%lu\n", counts->lines, counts->non_blank_lines);

	if (print_with_filename) printf(" ");
	printf("words:%lu longest-line:%lu avg/line:%.2f\n", counts->words, counts->longest_line_word, (float)counts->words / (float)counts->lines);

	if (print_with_filename) printf(" ");
	printf("bytes:%lu longest-line:%lu avg/line:%.2f\n", counts->bytes, counts->longest_line_byte, (float)counts->bytes / (float)counts->lines);

	if (print_with_filename) printf(" ");
	printf("KiB:%.2f MiB:%.7f\n", (float)counts->bytes / 1024, (float)counts->bytes / (1024 * 1024));
}

static void count(char *filename, int with, bool print_with_filename) {
	bool in_word = false;

	ulong i;

	struct counts counts = {0};

	ulong line_len_word = 0;
	ulong line_len_byte = 0;

	char *s = NULL;

	if (with == WITH_FILE) {
		s = fj_string_from_file(filename, s);
	} else if (with == WITH_STDIN) {
		s = fj_string_from_file_stream(stdin, s);
	}

	if (s == NULL) return;

	for (i = 0; i < fj_arr_length(s) - 1; ++i) {
		char ch = s[i];

		if (ch == ' ' || ch == '\n' || ch == '\t') {
			in_word = false;
		} else if (!in_word) {
			in_word = true;
			++counts.words;
			++line_len_word;
		}

		++line_len_byte;

		if (ch == '\n') {
			++counts.lines;

			if (line_len_byte != 1) {
				++counts.non_blank_lines;
			}

			if (counts.longest_line_byte < line_len_byte) counts.longest_line_byte = line_len_byte;
			if (counts.longest_line_word < line_len_word) counts.longest_line_word = line_len_word;
			line_len_word = 0;
			line_len_byte = 0;
		}

		++counts.bytes;
	}

	total.bytes += counts.bytes;
	total.words += counts.words;
	total.lines += counts.lines;
	total.non_blank_lines += counts.non_blank_lines;
	total.longest_line_byte += counts.longest_line_byte;
	total.longest_line_word += counts.longest_line_word;

	report(filename, print_with_filename, &counts);
}

int main(int argc, char **argv) {
	if (argc > 1) {
		int i;
		for (i = 1; i < argc; ++i) {
			count(argv[i], WITH_FILE, true);
			if (argc > 2) printf("\n");
		}

		if (argc > 2) {
			report("total", true, &total);
		}
	} else {
		count("stdin", WITH_STDIN, false);
	}

	return 0;
}
