# c
`c` is a weird kind of file and directory explorer.

WARNING: To get this thing to work you currently need to put this into your .bashrc file:

```
alias c="source c"
```

I don't know about other shells. I'm working on this issue, tho.

# Usage
Before you do anything, make sure to run `c cache` to make sure everything will work. This might take a while depending on how large your home directory is. The cache is always built manually, since I can't be fucked to write a cache validator, and it takes forever to build. If you use this as much as I do, than the time it takes to build the cache will save you more time in the long run.

Running `c` (after building the cache) will launch `fzf` with a list of all the directories in your $HOME. The directory you choose will be the one you `cd` into. Pretty simple stuff, but it's super useful to be able to jump anywhere with a few keystrokes. And it's wicked fast because of the cache.

Running `c f` (with the space) will let you search for all the _files_ under your $HOME directory. You will `cd` into the directory that contains the file you select. This is great if you know the file name, but not the directory name. It's also the reason why the cache takes so damn long to build.

Running `c o` will let you search all the files under $HOME and open them with the `o` program from tutils. You can select multiple files with the TAB key in `fzf`.

Along with the "global" options, you also have "local" options. `c l` will give you a list of all directories under your current on, `c ol` will open a file from your current directory, `c fl` will `cd` into a directory containing a file under your current directory.

You can also do `c --help` to list all the commands to remind you what they are.

# Workflow
Here's a basic workflow:

I want to get into my tutils directory to do some work there. I run `c` and type in "tuti" and press ENTER in `fzf`, which takes me to ~/code/tutils.

Something is wrong with my vimrc config, so I run `c o` and type in "vimrc" which loads my vimrc in my editor. When finished, I exit right back into ~/code/tutils.

I open up another terminal window and want to search for a piece of documentation I have stored somewhere. I know its name is something to do with shell scripts, but I don't know the directory name. I run `c f` and search for it there.

I'm finished with my work for the day, and I want to watch a movie. Sadly the movie file name is a pain in the ass to type out in my terminal, so I use `c` to navigate to my movie directory, and `c ol` to search through my list of movies and choose one.
