# o
`o` opens files.

It concatenates arguments and a newline seperated list of files from stdin, checks the file type, and launches a program with all the files as an argument.

More details below.

# Examples:

```
o droopy.md
o tim.png cool.jpg man.gif
find . -type f | o
cat episode-list | o new-episode.mkv
fzf -m | o
```

# How o picks a program to launch with
The first file in the "file list" is the file that is checked. The file list is made from `o`'s arguments and stdin. If you give `o` a newline separated list of files from stdin, than the first file in that list is checked; otherwise the first file checked is the first argument.

To add and change programs and conditionals, you're going to want to edit the source of `o`. There's a dead simple `case` statement that checks for file extensions and opens them depending on the first file in the file list.

(Make sure to call the program with the "openwith" function! Just follow the pattern if you don't understand the script that well.)

By default, if there is no extension found, `o` will try to open the files with $EDITOR; if $EDITOR is not found, it opens them with `vi`.

# Limitations
Currently, `o` can't open more than one file type. It's assumed that all the files in the file list can be opened by the same program. I'm trying to think up solutions to this problem.
