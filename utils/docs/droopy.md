- 01 - droopy
- 02 - The droopyfile
- 03 - Interactive parts
- 04 - Tips and use cases

# 01 - droopy
`droopy` is a command/program launcher made specifically for `dmenu`, and made to be launched from a keyboard shortcut in your window manager.

This isn't a normal program launcher, in the sense that it looks through everything in your $PATH to find all your programs. If you want that functionality, just use something like `dmenu_run` or `rofi`.

What makes `droopy` unique is it's configuration file.

(Warning: this thing is pretty jank, even by the standards of tutils. I find a lot of use out of `droopy`, but you probably won't. There are a _ton_ of weird quirks and edge cases and it's more or less a huge mess.)

# 02 - The droopyfile
By default `droopy` will look for a file called "droopy" inside the $HOME/.config directory. If you give it an argument that's a file path, it will use that as the "droopyfile" instead. If the droopyfile isn't there, than `droopy` won't do anything.

A droopyfile is, at its core, a list of programs to launch. The simplest droopyfile would look something like this:

```
xfce4-terminal
firefox
vlc
thunderbird
keepassxc
thunar
```

Used this way, it's no more than a program launcher where you explicitly choose what programs to use. Not very useful by it's own. Here's a better example of a droopyfile:

```
xfce4-terminal`tt
firefox`ff
thunderbird`email
thunar`file manager
```

That delimiter is the \` mark. Either called a "grave mark" or a "backtick". It's a pretty obscure character, which makes it perfect as a delimiter.

You can probably guess what would happen if you searched for "ff" in this dmenu prompt. It would choose and launch the `firefox` program.

One of the most important parts of `droopy` is it's ability to _tag_ commands. You can put arbitrary tags after a grave mark, so that your command list is easier to search for.

But that isn't all it can do, either.

```
# droopy can do comments
@`TERMINAL`xfce4-terminal -e
TERMINAL htop
TERMINAL cmus`music
TERMINAL newsboat`rss
TERMINAL bc -l`calculator
```

First, `droopy` understands that lines starting with \# are comments, and will be ignored entirely. _Comments do not work at the end of lines_, the only way to do comments is to have them at the beginning.

The more interesting bit is that second line. This is a _variable_. As you can probably guess, `droopy` will replace `TERMINAL` with `xfce4-terminal -e` in this case, which means that this is equivalent to launching a GUI app for things like `htop` and `bc -l`.

Even better, you can tag them to things like "music", "calculator", or whatever you want. So if you like to have `bc -l` as a calculator, and have it be easily searchable as "calculator", than this might be of use to you.

Just to drive home the point, here's some more examples:

```
@`!SCRIPT`~/code/shell
@`!TERM`urxvt -e
@`~/`/home/username

!SCRIPT/do-arbitrary-thing.sh
!SCRIPT/explode.pl
!TERM !SCRIPT/some-interactive-script.sh
```

A lot going on here. First off, you can use any characters besides \` as variable names and values.

Second, `droopy` has issues with expanding ~/ into $HOME, so it's usually good practice to have that third line there.

Third, variables are executed in the order they appear. So the last variable in this list replaces the ~/ in the result of the first variable. Reverse the order and the script wouldn't work.

Forth, `droopy` ignores blank lines.

In actuality, `droopy` actually does an extended regex search (`awk` style) and replace:

```
# format: @`regex`replacement
@`thuna[a-z]+`firefox
thunaaaa
```

Choosing the "thunaaaa" option will actually launch `firefox` in this instance. This seems like it's pretty powerful, but I've never actually found a use for it. It's mostly a side effect of the way the input string is parsed.

If you want to escape a character in a variable, than good luck:

```
@`tim\\\\[`firefox
tim[
```

Choosing the "tim[" option will launch `firefox` in this case. Yes those are _four_ back slashes. You can thank `awk` for that. Source: <https://stackoverflow.com/questions/25867060/awk-warning-escape-sequence-treated-as-plain>

I find that it's easier to just use variables that aren't complicated. Because the complexity, especially considering the fact that variables act on the substitutions of previous variables, would quickly spiral out of control. Things like "!TERM" work the best for me, here. But you can always come up with your own conventions.

I warned you this program was jank.

# 03 - Interactive parts
`droopy` uses `dmenu` by default. It's optimized for the various quirks of `dmenu`, so it's recommended to use `dmenu` or a 1:1 drop in replacement.

It _doesn't_ work with `fzf` right now, for various reasons including laziness (because of how `fzf` outputs queries) and because I can't seem to get programs to fork correctly when launched from the terminal. Currently: the only "supported" way to launch `droopy` is from your window manager.

`dmenu` allows you to search lines of input and select one. You type in parts of what you want to select, and press ENTER when the highlighted bit is over your selection.

```
firefox`ff
gimp`fire fox
```

If your search is "fire fox" (with the space), _both_ lines will still be on the screen, with the first line being the highlighted one. This is because the space character actually _seperates filters_ in `dmenu`.

What I mean is that you can imagine each space-separated string of characters -- a "word" -- to be its own search filter on the output of the previous word. So "fire fox" searches for "fire" and then takes the output of "fire" and searches for "fox".

This might seem stupid, but it's actually one of the most genius parts of `dmenu`. If you think about it in the term of filers, instead of an entire search string, you will realize that this allow you much more "human" control over your search; like if you have a bookmark that you know is about `vim`, but you don't know exactly, you just type in "vim", look at what results are there, and give it more filters until you have what you want.

Not everything is sunshine and daises, though. Imagine you have something like this:

```
@`~/`/home/username
xterm -e vim ~/.config/droopy
droopy ~/path/to/different/droopyfile
```

What do you think will happen if you give `dmenu` the search string "droopy"? Both lines here will make it through the filter, but the _second_ line will be the one on the top.

As far as I know, `dmenu`'s sorting algorithm gives strange priority to things at the beginning of the line. If you want it so that the "xterm..." line comes up first when you type in "droopy" in this case, you can do something like:

```
@`!!`
@`~/`/home/username
xterm -e vim ~/.config/droopy
!!droopy ~/path/to/different/droopyfile
```

This will make the sort work so that the "xterm..." line will be sorted better. You'll notice that the `!!` variable is empty, but still works. It won't have any impact on the execution of your script, but will have impact on interacting with `droopy`.

And if that isn't jank enough for you, there are also _arguments_ you can give commands.

When you have something selected with `dmenu` and press the TAB key, it'll fill out your query field with the entire selection without selecting it. Than if you type out extra stuff it'll be outputted the way you typed it, instead of just your selection.

This part is hard to explain, so I'll just show you:

```
xterm -e man`man page`
```

Imagine you selected this line and then pressed TAB. Now your query string is the entire line, with the cursor at the end. `droopy` understands arguments to commands, so all you have to do here is type in your argument and press ENTER, and you'll load a man page with that argument in another terminal window.

The way you tell `droopy` you want arguments, is to put an extra backtick at the end of your line, after the tag field. It can also work like this, if you don't want any tags:

```
xterm -e man``
```

Arguments in `droopy` are kind of limited, since there's no way (currently?) to tell it where to put the argument; it will always go at the end of your command whether you like it or not. If you absolutely need the argument to go somewhere else, the best work around is to write a small wrapper script and call that instead.

You can look into `man dmenu` to learn some more keyboard shortcuts to help with you selecting what you want.

# 04 - Tips and use cases
(This section under construction)

You can use `droopy` for:

- An internet bookmark manager
- A `tmux` manager
	- Launch a program in a new `tmux` pane/window/session
	- Change to a named session
	- Explode
- Something else that manages your windows with arbitrary commands
- Script launcher
	- Can even use it to give them arguments, like qr codes or something
- Generic program launcher
