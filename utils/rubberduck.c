/* Rubber duck programming helper! Insipred by a thing I saw on Harvard's CS50 course (The subreddit, not the actual course). */
#include <stdio.h>

int main(void) {
	int ch;

	printf("> ");

	for (;;) {
		ch = getchar();

		if (ch == '\n') {
			printf("\nRubber duck: quack\n\n");
			printf("> ");
		}

		if (ch == EOF) {
			printf("\n");
			break;
		}
	}

	return 0;
}
