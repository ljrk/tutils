/* timeuntil - calculate the time until a certain date */
/* Reports in your local timezone on your system clock */
/* Report format example: 19094d 10h 23m 50s */
/* Fun for the whole family! Set an unrelenting countdown towards your own demise! */

/* USAGE: timeuntil <year> <month> <day> <hour> <minute> <second> */

#define FJ_IMPL
#include "fj.h"

int main(int argc, char **argv) {
	int days = 0, hours = 0, mins = 0, secs = 0;
	struct tm tm_until = {0};
	time_t now = time(NULL);
	time_t until = now;
	double diff;
	long n;

	if (argc != 7) {
		printf("USAGE: timeuntil <year> <month> <day> <hour> <minute> <second>\n");
		return 0;
	}

	tm_until.tm_year = atoi(argv[1]) - 1900;
	tm_until.tm_mon = atoi(argv[2]) - 1;
	tm_until.tm_mday = atoi(argv[3]);
	tm_until.tm_hour = atoi(argv[4]);
	tm_until.tm_min = atoi(argv[5]);
	tm_until.tm_sec = atoi(argv[6]);

	until = mktime(&tm_until);
	diff = difftime(until, now);

	if (diff < 0) {
		fj_die("Current time exceeds destination time\n");
	}

	n = (long)diff;
	days = n / (24 * 3600);
	n = n % (24 * 3600);
	hours = n / 3600;
	n = n % 3600;
	mins = n / 60;
	n = n % 60;
	secs = n;

	printf("%dd %dh %dm %ds\n", days, hours, mins, secs);

	return 0;
}
