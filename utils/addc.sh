#!/bin/sh
# Just does the annoying pre-setup for a tutils c program
# puts the output program in gitignore, the install script, the build script, and makes the .c file
# addc.sh <program>

name="$1"

if [ -z "$name" ] ; then printf 'give me a name\n'; exit 1 ; fi

printf "$name\n" >> ../.gitignore
printf "cp $name \"\$bindir\"\n" >> install.sh
printf "gcc \$flags -o $name ${name}.c\n" >> build.sh
touch "${name}.c"
sed "s/@@NAME/$name/" meta/addc-template.txt > "$name.c"

printf "Made program \"$name\"\n"
