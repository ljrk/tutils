/* 
fj.h - Header-only utility library

SPDX-License-Identifier: 0BSD
(A public domain equivalent license. See the bottom of the file for details.)

www.timtimestim.com

 You have to define:

	#define FJ_IMPL

 in at least one source file before including fj.h. Like:

	#define FJ_IMPL
	#include "fj.h"

 Simply include fj.h normally with every other file.

Table of contents:
- Sizes
- Types
- Dynamic array
- String stuff
- RNG
- Misc
- Print debugging

@TODO:
 - handle malloc() and realloc() failures in a sane way
 - command line args
 - add FJ_NODEBUG to get rid of asserts and other things
 - handle trying to open a directory in fj_string_from_file() and fj_string_from_file_stream() (just return NULL?)
 - fj_char_is_whitespace(), fj_char_is_alpha(), fj_char_is_num(), fj_char_is_alpha_num(), fj_char_is_punctuation(), fj_char_is_graphical(), fj_char_is_control()
	Don't want to have to rely on the C local for these to be consistant. They're easy enough to implement anyways.
	Will only really work with ascii
 - Beginnings of platform layer (just for unix/POSIX at the moment):
	- fj_mkdir()
	- fj_pwd()
	- fj_is_directory()
	- fj_is_file()
	- fj_does_file_exist()
	- fj_list_directory()
	- fj_list_directory_recursive()
*/

#ifndef FJ_H
#define FJ_H

#include <assert.h>
#include <ctype.h>
#include <limits.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

/* Sizes */
#define FJ_BYTE 1UL
#define FJ_KIB 1024UL
#define FJ_MIB 1048576UL
#define FJ_GIB 1073742000UL

/* Types */
typedef int8_t s8;
typedef int16_t s16;
typedef int32_t s32;
typedef int64_t s64;
typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

typedef uintmax_t umax;
typedef intmax_t imax;

typedef unsigned int uint;
typedef unsigned long int ulong;
typedef unsigned short ushort;
typedef unsigned char uchar;
typedef signed char schar;

/* Dynamic array */

#define FJ_ARR_GROWTH_FACTOR 1.5

/* This struct is at the beginning of every dynamic array */
struct fj_a {
	size_t length; /* How many elements are in the array */
	size_t capacity; /* How many total elements will fit in the array */
};

/* WARNING!! These macros evaluate their arguments many times!! The only one that is "safe" is the "v" argument on fj_arr_push(a,v) */
#define fj_arr_head(a) ((struct fj_a *)(a) - 1)
#define fj_arr_length(a) ((a) != NULL ? (fj_arr_head(a))->length : 0)
#define fj_arr_capacity(a) ((a) != NULL ? (fj_arr_head(a))->capacity : 0)
#define fj_arr_add_length(a,n) (fj_arr_grow_when_needed(a, n), (fj_arr_head(a))->length += (n))
#define fj_arr_push(a,v) (fj_arr_grow_when_needed(a, 1), (a)[fj_arr_head(a)->length++] = (v))
#define fj_arr_pop(a) ((a)[fj_arr_head(a)->length -= 1])
#define fj_arr_last(a) ((a)[fj_arr_head(a)->length - 1])
#define fj_arr_last_index(a) (fj_arr_length(a) - 1)
#define fj_arr_set_length(a,n) ((fj_arr_capacity(a) < (n) ? fj_arr_grow(a, 0, n) : 0), (a) != NULL ? fj_arr_head(a)->length = (n) : 0)
#define fj_arr_add_capacity(a,n) (fj_arr_grow(a, fj_arr_capacity(a) + (n), 0))

#define fj_arr_grow_when_needed(a,n) (((a) == NULL || fj_arr_length(a) + (n) >= fj_arr_head(a)->capacity) ? fj_arr_grow(a, n, 0) : 0)
#define fj_arr_grow(a,n,m) ((*((void **)&(a)) = fj_arr_set((a), (n), sizeof(*(a)), (m))))

static void *fj_arr_set(void *arr, size_t items_to_grow, size_t item_size, size_t min_capacity);
static void fj_arr_free(void *arr);

/* String stuff */
static char *fj_string_from_file_stream(FILE *stream, char *buf);
static char *fj_string_from_file(char *filename, char *buf);
static u64 fj_string_to_unsigned(char *str);
static char *fj_string_next_char(char *str, u32 length_to_read, char char_to_stop_on);
static u32 fj_string_split_on_char(char *str, char *split_array[], char split_char, u32 max_fields, u32 max_len);
static bool fj_string_is_positive_int(char *str);

/* RNG */
struct fj_rand_state {
    u64 state;
    u64 inc;
};

static struct fj_rand_state fj_rand_state_global;

static void fj_rand_init(void);
static u32 fj_rand(void);

static void fj_rand_seed(struct fj_rand_state* rng, u64 initstate, u64 initseq);
static u32 fj_rand_gen(struct fj_rand_state* rng);

/* Misc */
#define fj_assert(x) if (x) {} else fj_die("ASSERT FAILED! %s:%d\n", __FILE__, __LINE__)

static size_t fj_file_length(FILE *file);
static void fj_die(char *format_string, ...);

/* Print debugging */
#define FJ_PP() printf("%s:%d\n", __FILE__, __LINE__)
#define FJ_PPV(p, v) printf("%s:%d(%" p ")\n", __FILE__, __LINE__, v)
#define FJ_PPVU(v) printf("%s:%d(%ju)\n", __FILE__, __LINE__, (umax)v)
#define FJ_PPVD(v) printf("%s:%d(%jd)\n", __FILE__, __LINE__, (imax)v)
#define FJ_PPVC(v) printf("%s:%d(%c)\n", __FILE__, __LINE__, v)
#define FJ_PPVF(v) printf("%s:%d(%f)\n", __FILE__, __LINE__, v)
#define FJ_PPVS(v) printf("%s:%d(%s)\n", __FILE__, __LINE__, v)
#define FJ_PPVP(v) printf("%s:%d(%p)\n", __FILE__, __LINE__, (void *)v)
#define FJ_PP_N() printf("%s:%d", __FILE__, __LINE__)
#define FJ_PPV_N(p, v) printf("%s:%d(%" p ")", __FILE__, __LINE__, v)

#endif /* FJ_H */

/******************************************************************************/

#ifdef FJ_IMPL

/* Dynamic array */

static void *fj_arr_set(void *arr, size_t items_to_grow, size_t item_size, size_t min_capacity) {
	size_t scaled_cap = arr != NULL ? FJ_ARR_GROWTH_FACTOR * fj_arr_head(arr)->capacity : 0;
	size_t required_cap = arr != NULL ? fj_arr_head(arr)->length + items_to_grow : items_to_grow;
	size_t next_cap = required_cap > scaled_cap ? required_cap : scaled_cap;

	struct fj_a *head;

	/* Always start at at least 2 len */
	if (next_cap <= 2) {
		next_cap = 2;
	}

	if (next_cap < min_capacity) {
		next_cap = min_capacity;
	}

	head = realloc((arr != NULL ? fj_arr_head(arr) : NULL), sizeof(struct fj_a) + (item_size * next_cap));

	if (head != NULL) {
		if (arr == NULL) {
			head->length = 0;
		}
		head->capacity = next_cap;

		return head + 1;
	} else {
		/* Will have to force a NULL realloc() return to test, most likley */
		return NULL;
	}
}

static void fj_arr_free(void *arr) {
	free(fj_arr_head(arr));
}

/* String stuff */

static char *fj_string_from_file_stream(FILE *stream, char *buf) {
	/* You can't really use fseek() or rewind() on single pass file streams like stdin, so you have to get the entire input in a single pass like this. */
	size_t fread_size;
	char *read_buf = NULL;
	size_t buf_size = 0;
	size_t read_buf_size = FJ_KIB * 4;

	fj_arr_set_length(read_buf, read_buf_size);

	while (!feof(stream)) {
		fread_size = fread(read_buf, 1, read_buf_size, stdin);
		fj_arr_set_length(buf, buf_size + fread_size + 1);
		memcpy(buf + buf_size, read_buf, fread_size);
		buf_size += fread_size;

		if (fread_size >= read_buf_size && read_buf_size < FJ_MIB) {
			read_buf_size *= 2;
			fj_arr_set_length(read_buf, read_buf_size);
		}
	}

	buf[buf_size] = '\0';

	fj_arr_free(read_buf);

	return buf;
}

static char *fj_string_from_file(char *filename, char *buf) {
	FILE *file = fopen(filename, "rb");
	size_t len;

	if (file == NULL) {
		if (buf != NULL) {
			fj_arr_free(buf);
		}

		return NULL;
	}

	len = fj_file_length(file);
	fj_arr_set_length(buf, len + 1);
	fread(buf, 1, len, file);
	buf[len] = '\0';

	fclose(file);

	return buf;
}

/* Expects null terminated, or ending in something not a number */
static u64 fj_string_to_unsigned(char *str) {
	u32 i;
	size_t n = 0;

	for (i = 0; str[i] >= '0' && str[i] <= '9'; ++i) {
		n = 10 * n + (str[i] - '0');
	}

	return n;
}

/* Also stops if it reaches a NUL value. Returns a NULL pointer if there's no match or nul terminator in length_to_read. */
static char *fj_string_next_char(char *str, u32 length_to_read, char char_to_stop_on) {
	u32 i;
	char *pos = NULL;

	for (i = 0; i < length_to_read; ++i) {
		if (str[i] == char_to_stop_on || str[i] == '\0') {
			pos = str + i;
			break;
		}
	}

	return pos;
}

/* Expects the string to be nul terminated. Returns how many splits there were (always at least 1, since the first pointer is a "field"). */
/* The string "str" is mutilated in the process of this function; each instance of "split_char" made into a null terminator. */
/* The "split_array" array gets a bunch of char pointers to the starting positions of each field */
static u32 fj_string_split_on_char(char *str, char *split_array[], char split_char, u32 max_fields, u32 max_len) {
	u32 split_count;

	char *p = str;
	u32 i = 0;

	split_array[0] = p;
	split_count = 1;

	while (i < max_len && *p != '\0') {
		if (*p == split_char) {
			if (split_count < max_fields) {
				split_array[split_count] = p + 1;
				str[i] = '\0';
			}

			++split_count;
		}

		++i;
		++p;
	}

	return split_count;
}

static bool fj_string_is_positive_int(char *str) {
	u32 i;
	bool is_valid = true;
	char c = '\0';

	for (i = 0; *(str + i) != '\0'; ++i) {
		c = *(str + i);
		if (!isdigit(c)) {
			is_valid = false;
			break;
		}
	}

	return is_valid;
}

/* RNG */

/* Based off of pcg32.
 fj_rand_init() inits the RNG state.
 fj_rand() gives you a random 32 bit unsigned number.
 Other functions used for fine control, if you need it.
 No need to call with any arguments. This is meant as a painless quick RNG.
 Keeps global state for the RNG. Probably not thread safe.
***********************************************************/

static u32 fj_rand(void) {
	return fj_rand_gen(&fj_rand_state_global);
}

static void fj_rand_init() {
	/* Tries to get enough entropy from the time and some addresses. If you're paranoid, obviously don't use this as a seed. But it should be good enough for most systems that need a simple RNG. */
	fj_rand_seed(&fj_rand_state_global, time(NULL) ^ (s64)&printf, (s64)&fj_rand_state_global);
}

static u32 fj_rand_gen(struct fj_rand_state *rng) {
	u32 xorshifted, rot;
    u64 oldstate = rng->state;
    rng->state = oldstate * 6364136223846793005ULL + rng->inc;
    xorshifted = ((oldstate >> 18u) ^ oldstate) >> 27u;
    rot = oldstate >> 59u;
    return (xorshifted >> rot) | (xorshifted << ((-rot) & 31));
}

static void fj_rand_seed(struct fj_rand_state *rng, u64 initstate, u64 initseq) {
    rng->state = 0U;
    rng->inc = (initseq << 1u) | 1u;
    fj_rand_gen(rng);
    rng->state += initstate;
    fj_rand_gen(rng);
}

/* Misc */

static size_t fj_file_length(FILE *file) {
	size_t len = 0;
	fseek(file, 0, SEEK_END);
	len = ftell(file);
	rewind(file);
	return len;
}

static void fj_die(char *format_string, ...) {
	va_list args;
	va_start(args, format_string);
	vfprintf(stderr, format_string, args);
	exit(1);
}

#endif /* FJ_IMPL */

/*
------------------------------------------------------------------------
License - 0BSD
------------------------------------------------------------------------
Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
------------------------------------------------------------------------
*/
