#!/bin/sh
# Build all of the c programs in this here thing

flags='-g -std=c99 -Wall -Wextra -pedantic -Wmissing-prototypes -Wstrict-prototypes -Wno-unused-function'

if [ "$1" = "release" ] ; then
	flags='-O3 -std=c99 -Wall -Wextra -pedantic -Wmissing-prototypes -Wstrict-prototypes -Wno-unused-function'
	echo "building release..."
else
	echo "building debug..."
fi


gcc $flags -o rubberduck rubberduck.c
gcc $flags -o wc2 wc2.c
gcc $flags -o timeuntil timeuntil.c
