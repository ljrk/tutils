#!/bin/sh
# quickly make a shell script program
# addsh.sh <program>

name="$1"

if [ -z "$name" ] ; then printf 'give me a name\n'; exit 1 ; fi

printf "cp $name \"\$bindir\"\n" >> install.sh
sed "s/@@NAME/$name/" meta/addsh-template.txt > "$name"
chmod +x "${name}"

printf "Made program \"$name\"\n"
